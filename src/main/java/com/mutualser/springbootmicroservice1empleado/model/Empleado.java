package com.mutualser.springbootmicroservice1empleado.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
@Entity
@Table(name="empleados")
public class Empleado {

    public Empleado() {

    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name="nombre", length = 150, nullable = false )
    private String nombre;

    @NotBlank
    @Column(name="apellidos", length=150, nullable=false)
    private String apellidos;

    @NotNull
    @Column(name="sexo", length=1, nullable=false)
    private int sexo;

    @NotNull
    @Column(name="fecha_nacimiento", nullable=false)
    private LocalDate fechaNacimiento;

    @NotBlank
    @Column(name="correo_electronico", nullable = false)
    private String correo_electronico;





}
