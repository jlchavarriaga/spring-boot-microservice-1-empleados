package com.mutualser.springbootmicroservice1empleado.controller;

import com.mutualser.springbootmicroservice1empleado.model.Empleado;
import com.mutualser.springbootmicroservice1empleado.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Map;
import java.util.HashMap;
import java.time.LocalDate;
import java.time.Period;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/empleado")
public class EmpleadoController {

    @Autowired
    private EmpleadoService empleadoService;

    /**
     * Guarda un empleado.
     *
     * Este endpoint permite guardar un empleado en el sistema.
     *
     * @param empleado El objeto Empleado a guardar (debe pasar las validaciones).
     * @return ResponseEntity con el empleado guardado y el código de estado HTTP 201 si la solicitud es exitosa.
     *
     *     Ejemplo: http://localhost:3333/api/empleado/
     **/
    @PostMapping
    public ResponseEntity<?> saveEmpleado(@RequestBody Empleado empleado){
        return new ResponseEntity<>(
                empleadoService.saveEmpleado(empleado),
                HttpStatus.CREATED
        );
    }


    //http://localhost:3333/api/empleado/2
    @DeleteMapping("{empleadoId}")
    public ResponseEntity<?> deleteEmpleado(@PathVariable Long empleadoId){
        empleadoService.deleteEmpleado(empleadoId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Obtiene una lista de empleados.
     *
     * Este endpoint permite obtener una lista de empleados registrados en el sistema.
     * La cantidad de empleados devueltos está limitada a 30 para cumplir con los requisitos especificados.
     *
     * @return ResponseEntity con la lista de empleados (limitada a 30) y el código de estado HTTP 200 si la solicitud es exitosa.
     *
     *  Ejemplo: http://localhost:3333/api/empleado/
     *
     */
    @GetMapping
    public ResponseEntity<?> getAllEmpleados(){

        // Obtener todos
       // return ResponseEntity.ok(empleadoService.findAllEmpleados());

        List<Empleado> empleados = empleadoService.findAllEmpleados();

        // Limitamos la cantidad de empleados a 30
        if (empleados.size() > 30) {
            empleados = empleados.subList(0, 30);
        }

        return ResponseEntity.ok(empleados);
    }


    /**
     * Actualiza los datos de un empleado existente.
     *
     * @param empleadoId El ID del empleado que se va a actualizar.
     * @param empleado   Los nuevos datos del empleado proporcionados en el cuerpo de la solicitud.
     * @return ResponseEntity con el empleado actualizado y el código de estado correspondiente.
     *
     * Ejemplo: http://localhost:3333/api/empleado/3
     */
    @PutMapping("/{empleadoId}")
    public ResponseEntity<?> updateEmpleado(@PathVariable Long empleadoId, @RequestBody Empleado empleado) {
        // Verificar si el empleado con el ID dado existe
        Optional<Empleado> existingEmpleado = empleadoService.findEmpleadoById(empleadoId);

        if (existingEmpleado.isPresent()) {
            // Obtenemos el empleado existente
            Empleado empleadoExistente = existingEmpleado.get();

            empleadoExistente.setNombre(empleado.getNombre());
            empleadoExistente.setApellidos(empleado.getApellidos());
            empleadoExistente.setSexo(empleado.getSexo());
            empleadoExistente.setFechaNacimiento(empleado.getFechaNacimiento());
            empleadoExistente.setCorreo_electronico(empleado.getCorreo_electronico());

            Empleado updatedEmpleado = empleadoService.saveEmpleado(empleadoExistente);

            return ResponseEntity.ok(updatedEmpleado);
        } else {
            // mensaje de error personalizado
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "100");
            errorResponse.put("message", "Empleado no encontrado");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }


    // Método para calcular la edad a partir de la fecha de nacimiento
    private int calcularEdad(LocalDate fechaNacimiento) {
        // Obtener la fecha actual
        LocalDate fechaActual = LocalDate.now();

        // Calcular el período entre la fecha de nacimiento y la fecha actual
        Period periodo = Period.between(fechaNacimiento, fechaActual);

        // Obtener el número de años del período
        int edad = periodo.getYears();

        return edad;
    }


    /**
     * Obtiene una lista de empleados con edad mayor o igual a 40 años.
     *
     * Este endpoint permite consultar una lista de empleados cuya edad sea igual o superior a 40 años
     * en función de su fecha de nacimiento.
     *
     * @return ResponseEntity con la lista de empleados que cumplen el criterio y el código de estado HTTP 200 si la solicitud es exitosa.
     */
    @GetMapping("/mayores-de-40")
    //@ApiOperation(value = "Empleados mayores de 40", notes = "Obtiene una lista de empleados con edad mayor o igual a 40 años.")
    public ResponseEntity<List<Empleado>> getEmpleadosMayoresDe40() {
        // Obtiene la lista completa de empleados desde el servicio
        List<Empleado> todosLosEmpleados = empleadoService.findAllEmpleados();

        // Filtra la lista para obtener solo los empleados mayores de 40 años
        List<Empleado> empleadosMayoresDe40 = todosLosEmpleados.stream()
                .filter(empleado -> calcularEdad(empleado.getFechaNacimiento()) >= 40)
                .collect(Collectors.toList());

        return ResponseEntity.ok(empleadosMayoresDe40);
    }


    /**
     * Obtiene una lista de empleados con sexo femenino.
     *
     * Este endpoint permite consultar una lista de empleados cuyo sexo sea femenino.
     *
     * @return ResponseEntity con la lista de empleados que cumplen el criterio y el código de estado HTTP 200 si la solicitud es exitosa.
     */
    @GetMapping("/femeninos")
    // @ApiOperation(value = "Empleados femeninos", notes = "Obtiene una lista de empleados con sexo femenino.")
    public ResponseEntity<List<Empleado>> getEmpleadosFemeninos() {
        // Obtiene la lista completa de empleados desde el servicio
        List<Empleado> todosLosEmpleados = empleadoService.findAllEmpleados();

        // Filtra la lista para obtener solo los empleados con sexo femenino
        List<Empleado> empleadosFemeninos = todosLosEmpleados.stream()
                .filter(empleado -> empleado.getSexo() == 0)
                .collect(Collectors.toList());

        return ResponseEntity.ok(empleadosFemeninos);
    }
}
