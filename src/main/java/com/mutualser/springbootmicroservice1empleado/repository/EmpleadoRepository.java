package com.mutualser.springbootmicroservice1empleado.repository;

import com.mutualser.springbootmicroservice1empleado.model.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpleadoRepository extends JpaRepository<Empleado, Long>{

}
