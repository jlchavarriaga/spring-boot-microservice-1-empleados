package com.mutualser.springbootmicroservice1empleado.service;

import com.mutualser.springbootmicroservice1empleado.model.Empleado;
import com.mutualser.springbootmicroservice1empleado.repository.EmpleadoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpleadoServiceImpl implements EmpleadoService{

    private final EmpleadoRepository empleadoRepository;


    public EmpleadoServiceImpl(EmpleadoRepository empleadoRepository) {
        this.empleadoRepository = empleadoRepository;
    }

    @Override
    public Empleado saveEmpleado(Empleado empleado){
        //empleado.setFechaNacimiento(LocalDate.now());
        return empleadoRepository.save(empleado);
    }

    @Override
    public void deleteEmpleado(Long empleadoId){
        empleadoRepository.deleteById(empleadoId);
    }

    @Override
    public List<Empleado> findAllEmpleados(){
        return empleadoRepository.findAll();
    }

    @Override
    public Optional<Empleado> findEmpleadoById(Long empleadoId) {
        return empleadoRepository.findById(empleadoId);
    }

}
