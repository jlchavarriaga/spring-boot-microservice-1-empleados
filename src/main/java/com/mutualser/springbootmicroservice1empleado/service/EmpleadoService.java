package com.mutualser.springbootmicroservice1empleado.service;

import com.mutualser.springbootmicroservice1empleado.model.Empleado;

import java.util.List;
import java.util.Optional;

public interface EmpleadoService {

    Empleado saveEmpleado(Empleado empleado);

    void deleteEmpleado(Long empleadoId);

    List<Empleado> findAllEmpleados();

    Optional<Empleado> findEmpleadoById(Long empleadoId);
}
