package com.mutualser.springbootmicroservice1empleado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMicroservice1EmpleadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMicroservice1EmpleadoApplication.class, args);
	}

}
